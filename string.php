<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>STring PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php
    echo "<h3> Soal No 1</h3>";
        $kalimat1 = "Hello PHP!";
        echo "kalimat : " . $kalimat1 . "<br>";
        echo "panjang karakter kalimat : " . strlen($kalimat1) . "<br>";
        echo "jumlah kata kalimat : " . str_word_count($kalimat1) . "<br>" . "<br>";

        $kalimat2 = "i'm ready for the challenges";
        echo "kalimat : " . $kalimat2 . "<br>";
        echo "panjang karakter kalimat : " . strlen($kalimat2) . "<br>";
        echo "jumlah kata kalimat : " . str_word_count($kalimat2) . "<br>" . "<br>";

        echo "<h3> Soal No 2</h3>";
        $string2 = "I love PHP";
        echo "Kalimat Kedua : " . $string2 . "<br>";
        echo "Kata Pertama : " . substr($string2,0,1) . "<br>";
        echo "Kata Kedua : " . substr($string2,2,4) . "<br>";
        echo "Kata Ketiga : " . substr($string2,7,3) . "<br>" . "<br>";

        echo "<h3> Soal No 3</h3>";
        $string3 = "PHP is old but sexy!";
        echo "Kalimat ketiga : " . $string3 . "<br>";
        echo "Ganti kalimat : " . str_replace("sexy","awesome",$string3);
    ?>
</body>
</html>